data "archive_file" "test-lambdas" {
  type        = "zip"
  output_path = "test-lambdas.zip"
  source_dir  = "${path.module}/src"
}
